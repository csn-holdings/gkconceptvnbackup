<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

require_once JPATH_SITE . '/plugins/system/route66/lib/rule.php';

class Route66RuleVirtuemartCategory extends Route66Rule
{
	private static $cache = array();
	protected $variables = array('option' => 'com_virtuemart', 'view' => 'category', 'virtuemart_category_id' => '@', 'id' => '');

	public function getTokensValues($query)
	{
		if (!isset($query['virtuemart_category_id']))
		{
			$query['virtuemart_category_id'] = 0;
		}

		// Cache key
		$key = (int) $query['virtuemart_category_id'];

		// Check cache
		if (isset(self::$cache[$key]))
		{
			return self::$cache[$key];
		}

		// Get database
		$db = JFactory::getDbo();

		// Get query
		$dbQuery = $db->getQuery(true);

		// Initialize values
		$values = array();

		// Iterate over the tokens
		foreach ($this->tokens as $token)
		{

			// ID
			if ($token == '{categoryId}')
			{
				$values[] = (int) $query['virtuemart_category_id'];
				$dbQuery->select($db->qn('virtuemart_category_id'));
			}
			// Alias
			elseif ($token == '{categoryAlias}')
			{
				if (strpos($query['virtuemart_category_id'], ':'))
				{
					$parts = explode(':', $query['virtuemart_category_id']);
					$values[] = $parts[1];
				}
				$dbQuery->select($db->qn('slug'));
			}
			// Path
			elseif ($token == '{categoryPath}')
			{
				JModelLegacy::addIncludePath(JPATH_SITE . '/plugins/route66/virtuemart/models');
				$model = JModelLegacy::getInstance('Virtuemart', 'Route66Model', array('ignore_request' => true));
				$category = $model->getCategory((int) $query['virtuemart_category_id']);
				$categoryPath = $model->getCategoryPath($category->virtuemart_category_id, $category->category_parent_id, array($category->slug));
				$dbQuery->select($db->q($categoryPath));
			}
		}

		// Check if we already have what we need
		if (count($this->tokens) === count($values))
		{
			self::$cache[$key] = $values;

			return $values;
		}

		// If not let's query the database
		$dbQuery->from($db->qn('#__virtuemart_categories_' . VmConfig::$vmlang));
		$dbQuery->where($db->qn('virtuemart_category_id') . ' = ' . (int) $query['virtuemart_category_id']);
		$db->setQuery($dbQuery);
		$values = $db->loadRow();
		self::$cache[$key] = $values;

		return $values;
	}

	public function getQueryValue($key, $tokens)
	{
		if ($key == 'virtuemart_category_id')
		{
			// First check that ID is not already in the URL
			if (isset($tokens['{categoryId}']))
			{
				return $tokens['{categoryId}'];
			}

			// Check for alias
			if (isset($tokens['{categoryAlias}']))
			{
				return $this->getCategoryIdFromAlias($tokens['{categoryAlias}']);
			}

			// Check for path
			if (isset($tokens['{categoryPath}']))
			{
				return $this->getCategoryIdFromPath($tokens['{categoryPath}']);
			}
		}
		else
		{
			return;
		}
	}

	public function getItemid($variables)
	{
		if (!function_exists('virtuemartBuildRoute'))
		{
			include_once JPATH_SITE . '/components/com_virtuemart/router.php';
		}
		virtuemartBuildRoute($variables);
		$Itemid = isset($variables['Itemid']) ? $variables['Itemid'] : '';

		return $Itemid;
	}

	private function getCategoryIdFromAlias($alias)
	{
		if (strpos($alias, '/') !== false)
		{
			$parts = explode('/', $alias);
			$alias = end($parts);
		}
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->qn('virtuemart_category_id'))->from($db->qn('#__virtuemart_categories_' . VmConfig::$vmlang))->where($db->qn('slug') . ' = ' . $db->q($alias));
		$db->setQuery($query);
		$id = $db->loadResult();

		return $id;
	}

	private function getCategoryIdFromPath($path)
	{
		$parts = explode('/', $path);
		$alias = end($parts);

		return $this->getCategoryIdFromAlias($alias);
	}
}
