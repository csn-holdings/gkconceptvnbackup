<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

class Route66ModelVirtuemart extends JModelLegacy
{
	private static $cache = array();

	public function getSitemapItems()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('product.virtuemart_product_id');
		$query->select('product.product_parent_id');
		$query->from($db->qn('#__virtuemart_products', 'product'));
		$query->select('productData.product_name');
		$query->leftJoin($db->qn('#__virtuemart_products_' . VmConfig::$vmlang, 'productData') . ' ON ' . $db->qn('product.virtuemart_product_id') . ' = ' . $db->qn('productData.virtuemart_product_id'));
		$query->where($db->qn('product.published') . ' = 1');
		$query->leftJoin($db->qn('#__virtuemart_product_categories', 'xref') . ' ON ' . $db->qn('product.virtuemart_product_id') . ' = ' . $db->qn('xref.virtuemart_product_id'));

		if ($this->getState('categories'))
		{
			$query->where($db->qn('xref.virtuemart_category_id') . ' IN(' . implode(',', $this->getState('categories')) . ')');
		}
		else
		{
			$query->where($db->qn('xref.virtuemart_category_id') . ' IS NOT NULL');
		}
		$query->order($db->qn('product.virtuemart_product_id'));
		$db->setQuery($query, $this->getState('offset'), $this->getState('limit'));
		$items = $db->loadObjectList();

		return $items;
	}

	public function countSitemapItems()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('COUNT(product.virtuemart_product_id)');
		$query->from($db->qn('#__virtuemart_products', 'product'));
		$query->where($db->qn('product.published') . ' = 1');
		$query->leftJoin($db->qn('#__virtuemart_product_categories', 'xref') . ' ON ' . $db->qn('product.virtuemart_product_id') . ' = ' . $db->qn('xref.virtuemart_product_id'));

		if ($this->getState('categories'))
		{
			$query->where($db->qn('xref.virtuemart_category_id') . ' IN(' . implode(',', $this->getState('categories')) . ')');
		}
		else
		{
			$query->where($db->qn('xref.virtuemart_category_id') . ' IS NOT NULL');
		}
		$db->setQuery($query);

		return $db->loadResult();
	}

	public function getProductImages($ids)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->qn('media.file_title') . ',' . $db->qn('media.file_url'))->from($db->qn('#__virtuemart_medias', 'media'));
		$query->where($db->qn('media.file_is_product_image') . ' = 1');
		$query->where($db->qn('media.file_type') . ' = ' . $db->q('product'));
		$query->where($db->qn('media.published') . ' = 1');
		$query->leftJoin($db->qn('#__virtuemart_product_medias', 'xref') . ' ON ' . $db->qn('media.virtuemart_media_id') . ' = ' . $db->qn('xref.virtuemart_media_id'));
		$query->select($db->qn('xref.virtuemart_product_id'));
		$query->where($db->qn('xref.virtuemart_product_id') . ' IN (' . implode(',', $ids) . ')');
		$query->order($db->qn('xref.ordering'));
		$db->setQuery($query);

		return $db->loadObjectList();
	}

	public function getProductCategories($ids)
	{
		$ids = array_unique($ids);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('virtuemart_product_id');
		$query->select('virtuemart_category_id');
		$query->from($db->qn('#__virtuemart_product_categories'));
		$query->where('virtuemart_product_id IN (' . implode(',', $ids) . ')');
		$query->group('virtuemart_product_id');
		$query->order('ordering');
		$db->setQuery($query);

		return $db->loadAssocList('virtuemart_product_id');
	}

	public function getCategoryPath($id, $parent, $path)
	{
		$parent = (int) $parent;

		if (!$parent)
		{
			return implode('/', array_reverse($path));
		}
		$category = $this->getCategory($parent);
		$path[] = $category->slug;

		if ($category->category_parent_id > 1)
		{
			return $this->getCategoryPath($category->virtuemart_category_id, $category->category_parent_id, $path);
		}
		else
		{
			$result = implode('/', array_reverse($path));

			return $result;
		}
	}

	public function getCategory($id)
	{
		$key = __FUNCTION__ . '_' . implode('_', func_get_args());

		if (!isset(self::$cache[$key]))
		{
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('category.virtuemart_category_id');
			$query->select('category.slug');
			$query->select('xref.category_parent_id');
			$query->from($db->qn('#__virtuemart_categories_' . VmConfig::$vmlang, 'category'));
			$query->leftJoin($db->qn('#__virtuemart_category_categories', 'xref') . ' ON ' . $db->qn('category.virtuemart_category_id') . ' = ' . $db->qn('xref.id'));
			$query->where($db->qn('category.virtuemart_category_id') . ' = ' . $id);
			$db->setQuery($query);
			self::$cache[$key] = $db->loadObject();
		}

		return self::$cache[$key];
	}

	public function getCategoryIdFromProductId($productId)
	{
		$key = __FUNCTION__ . '_' . implode('_', func_get_args());

		if (!isset(self::$cache[$key]))
		{
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select($db->qn('virtuemart_category_id'))->from($db->qn('#__virtuemart_product_categories'))->where($db->qn('virtuemart_product_id') . ' = ' . (int) $productId)->order($db->qn('ordering'));
			$db->setQuery($query, 0, 1);
			self::$cache[$key] = $db->loadResult();

			if (!self::$cache[$key])
			{
				$query = $db->getQuery(true);
				$query->select($db->qn('product_parent_id'))->from($db->qn('#__virtuemart_products'))->where($db->qn('virtuemart_product_id') . ' = ' . (int) $productId);
				$db->setQuery($query, 0, 1);
				$product_parent_id = $db->loadResult();

				if ($product_parent_id)
				{
					$query = $db->getQuery(true);
					$query->select($db->qn('virtuemart_category_id'))->from($db->qn('#__virtuemart_product_categories'))->where($db->qn('virtuemart_product_id') . ' = ' . (int) $product_parent_id)->order($db->qn('ordering'));
					$db->setQuery($query, 0, 1);
					self::$cache[$key] = $db->loadResult();
				}
			}
		}

		return self::$cache[$key];
	}

	public function getCategorySlug($categoryId)
	{
		$key = __FUNCTION__ . '_' . implode('_', func_get_args());

		if (!isset(self::$cache[$key]))
		{
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select($db->qn('slug'))->from($db->qn('#__virtuemart_categories_' . VmConfig::$vmlang))->where($db->qn('virtuemart_category_id') . ' = ' . (int) $categoryId);
			$db->setQuery($query);
			self::$cache[$key] = $db->loadResult();
		}

		return self::$cache[$key];
	}
}
