<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

use Joomla\CMS\Filesystem\File;

require_once JPATH_SITE . '/plugins/system/route66/lib/plugin.php';

class plgRoute66K2 extends Route66Plugin
{
	protected $rules = array('item', 'category', 'user', 'tag');

	public function __construct(&$subject, $config = array())
	{
		parent::__construct($subject, $config);

		if (!file_exists(JPATH_SITE . '/components/com_k2/k2.php'))
		{
			$this->rules = array();
		}
		elseif (!class_exists('K2HelperRoute'))
		{
			include_once JPATH_SITE . '/components/com_k2/helpers/utilities.php';
			include_once JPATH_SITE . '/components/com_k2/helpers/route.php';
		}
	}

	public function getSitemapItems($feed, $offset, $limit)
	{

		// Get model
		$model = $this->getModel();
		$model->setState('offset', $offset);
		$model->setState('limit', $limit);

		if ($feed->settings->get('type') == 'news')
		{
			$model->setState('offset', 0);
			$model->setState('limit', 0);
			$model->setState('filter.days', 2);
		}

		// Set category filter
		if ($feed->sources->get('k2') == 2 && is_array($feed->sources->get('k2Categories')) && count($feed->sources->get('k2Categories')))
		{
			$model->setState('categories', $feed->sources->get('k2Categories'));
		}

		$items = $model->getSitemapItems();
		$application = JFactory::getApplication();
		$timezone = new DateTimeZone($application->get('offset'));
		$ssl = $application->get('force_ssl') == 2 ? 1 : 2;
		require_once JPATH_SITE . '/components/com_k2/helpers/route.php';

		foreach ($items as $item)
		{
			$item->images = array();
			$item->videos = array();
			$link = K2HelperRoute::getItemRoute($item->id . ':' . $item->alias, $item->catid);

			if ($item->language && $item->language !== '*' && JLanguageMultilang::isEnabled())
			{
				$link .= '&lang=' . $item->language;
			}
			$item->url = JRoute::_($link, true, $ssl);

			if ($feed->settings->get('type') != 'news' && $feed->settings->get('images'))
			{
				$key = md5('Image' . $item->id);

				if (File::exists(JPATH_SITE . '/media/k2/items/cache/' . $key . '_XL.jpg'))
				{
					$image = new stdClass();
					$image->url = JUri::root(false) . 'media/k2/items/cache/' . $key . '_XL.jpg';
					$image->caption = $item->image_caption;
					$item->images[] = $image;
				}
			}

			if ($feed->settings->get('type') != 'news' && $feed->settings->get('videos'))
			{
				$video = $model->getVideo($item->video);

				if (($video && ($video->player || $video->location)) && count($item->images))
				{
					$video->thumbnail = $item->images[0]->url;
					$video->title = $item->title;
					$video->description = $item->video_caption;
					$item->videos[] = $video;
				}
			}

			$publicationDate = JFactory::getDate($item->publish_up);
			$publicationDate->setTimeZone($timezone);
			$item->publicationDate = $publicationDate->toISO8601(true);
		}

		return $items;
	}

	public function countSitemapItems($feed)
	{

		// Get model
		$model = $this->getModel();

		if ($feed->settings->get('type') == 'news')
		{
			$model->setState('filter.days', 2);
		}

		// Set category filter
		if ($feed->sources->get('k2') == 2 && is_array($feed->sources->get('k2Categories')) && count($feed->sources->get('k2Categories')))
		{
			$model->setState('categories', $feed->sources->get('k2Categories'));
		}

		return $model->countSitemapItems();
	}

	public function getInstantArticles($feed)
	{
		// Get model
		$model = $this->getModel();

		// Set category filter
		if ($feed->sources->get('k2') == 2 && is_array($feed->sources->get('k2Categories')) && count($feed->sources->get('k2Categories')))
		{
			$model->setState('categories', $feed->sources->get('k2Categories'));
		}

		// Fetch items
		$items = $model->getInstantArticles();

		// Set some variables
		$application = JFactory::getApplication();
		$ssl = $application->get('force_ssl') == 2 ? 1 : 2;
		$timezone = new DateTimeZone($application->get('offset'));
		$languageParams = JComponentHelper::getParams('com_languages');
		$siteLanguage = $languageParams->get('site');
		$params = JComponentHelper::getParams('com_k2');
		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_k2/tables');

		foreach ($items as $item)
		{

			// Get category
			$category = JTable::getInstance('K2Category', 'Table');
			$category->load($item->catid);

			// Build params
			$categoryParams = new JRegistry($category->params);
			$itemParams = new JRegistry($item->params);
			$item->params = clone $params;

			if ($categoryParams->get('inheritFrom'))
			{
				$masterCategoryID = $categoryParams->get('inheritFrom');
				$masterCategory = JTable::getInstance('K2Category', 'Table');
				$masterCategory->load((int) $masterCategoryID);
				$categoryParams = new JRegistry($masterCategory->params);
			}
			$item->params->merge($categoryParams);
			$item->params->merge($itemParams);

			// Item GUID
			$item->guid = md5(JUri::root(false) . '.com_k2.item.' . $item->id);

			// Item URL
			$item->url = JRoute::_(K2HelperRoute::getItemRoute($item->id . ':' . $item->alias, $item->catid), true, $ssl);

			// Text
			$item->text = '';

			if ($item->params->get('itemIntroText'))
			{
				$item->text .= $item->introtext;
			}

			if ($item->params->get('itemFullText'))
			{
				$item->text .= $item->fulltext;
			}

			// Image
			$item->image = null;
			$key = md5('Image' . $item->id);

			if ($item->params->get('itemImage') && File::exists(JPATH_SITE . '/media/k2/items/cache/' . $key . '_XL.jpg'))
			{
				$image = new stdClass();
				$image->src = JUri::root(false) . 'media/k2/items/cache/' . $key . '_XL.jpg';
				$image->caption = $item->image_caption;
				$item->image = $image;
			}

			if (is_null($item->image))
			{
				$image = $this->getFirstImage($item->text);
			}

			if (!$item->params->get('itemImageMainCaption') && $item->image)
			{
				$item->image->caption = '';
			}

			// Content plugins
			$item->text = JHtml::_('content.prepare', $item->text);

			// Prepare Instant Articles markup
			$item->text = $this->prepareTextForInstantArticles($item->text);

			// Video
			$video = null;

			if ($item->params->get('itemVideo') && $item->video)
			{
				$video = $model->getVideo($item->video);

				if ($video)
				{
					$video->title = $item->video_caption;
				}
			}
			$item->video = $video;

			// Gallery
			$item->gallery = array();

			if ($item->params->get('itemImageGallery'))
			{
				$item->gallery = $model->getGallery($item->id, $item->language);
			}

			// Author
			if ($item->params->get('itemAuthor'))
			{
				if ($item->created_by_alias)
				{
					$item->author = $item->created_by_alias;
				}
				else
				{
					$item->author = JUser::getInstance($item->created_by)->name;
				}
			}

			// Related items
			if ($item->params->get('itemRelated'))
			{
				$item->related = $model->getRelatedItems($item->id);

				foreach ($item->related as $related)
				{
					$related->url = JRoute::_(K2HelperRoute::getItemRoute($related->id . ':' . $related->alias, $related->catid), true, $ssl);
				}
			}

			// Publication and modification dates
			$publicationDate = JFactory::getDate($item->publish_up);
			$publicationDate->setTimeZone($timezone);
			$item->publicationDate = $publicationDate->toISO8601(true);

			if ((int) $item->modified)
			{
				$modificationDate = JFactory::getDate($item->modified);
				$modificationDate->setTimeZone($timezone);
				$item->modificationDate = $modificationDate->toISO8601(true);
			}
			else
			{
				$item->modificationDate = $item->publicationDate;
			}

			// Language
			if ($item->language == '*')
			{
				$item->language = $siteLanguage;
			}
			$item->language = strtolower($item->language);
		}

		return $items;
	}

	public function onRoute66IsExtensionInstalled()
	{
		$component = JComponentHelper::getComponent('com_k2');

		return (int) $component->id;
	}
}
