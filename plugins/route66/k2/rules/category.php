<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

require_once JPATH_SITE . '/plugins/system/route66/lib/rule.php';

class Route66RuleK2Category extends Route66Rule
{
	private static $cache = array();
	protected $variables = array('option' => 'com_k2', 'view' => 'itemlist', 'task' => 'category', 'id' => '@');

	public function getTokensValues($query)
	{
		// Cache key
		$key = (int) $query['id'];

		// Check cache
		if (isset(self::$cache[$key]))
		{
			return self::$cache[$key];
		}

		// Get database
		$db = JFactory::getDbo();

		// Get query
		$dbQuery = $db->getQuery(true);

		// Initialize values
		$values = array();

		// Iterate over the tokens
		foreach ($this->tokens as $token)
		{

			// ID
			if ($token == '{categoryId}')
			{
				$values[] = (int) $query['id'];
				$dbQuery->select($db->qn('id'));
			}
			// Alias
			elseif ($token == '{categoryAlias}')
			{
				if (strpos($query['id'], ':'))
				{
					$parts = explode(':', $query['id']);
					$values[] = $parts[1];
				}
				$dbQuery->select($db->qn('alias'));
			}
		}

		// Check if we already have what we need
		if (count($this->tokens) === count($values))
		{
			self::$cache[$key] = $values;

			return $values;
		}

		// If categoryPath token is present we need more data
		if (in_array('{categoryPath}', $this->tokens))
		{
			$dbQuery->select($db->qn('parent'));

			if (!in_array('{categoryId}', $this->tokens))
			{
				$dbQuery->select($db->qn('id'));
			}

			if (!in_array('{categoryAlias}', $this->tokens))
			{
				$dbQuery->select($db->qn('alias'));
			}
		}

		// Query the database
		$dbQuery->from($db->qn('#__k2_categories'));
		$dbQuery->where($db->qn('id') . ' = ' . (int) $query['id']);
		$db->setQuery($dbQuery);
		$row = $db->loadObject();

		$values = array();

		foreach ($this->tokens as $token)
		{
			if ($token == '{categoryId}')
			{
				$values[] = $row->id;
			}
			elseif ($token == '{categoryAlias}')
			{
				$values[] = $row->alias;
			}
			elseif ($token == '{categoryPath}')
			{
				JModelLegacy::addIncludePath(JPATH_SITE . '/plugins/route66/k2/models');
				$model = JModelLegacy::getInstance('K2', 'Route66Model', array('ignore_request' => true));
				$path = (int) $row->parent ? $model->getCategoryPath($row->id, $row->parent, array($row->alias)) : $row->alias;
				$values[] = $path;
			}
		}
		self::$cache[$key] = $values;

		return $values;
	}

	public function getQueryValue($key, $tokens)
	{
		if ($key == 'id')
		{
			// First check that ID is not already in the URL
			if (isset($tokens['{categoryId}']))
			{
				return $tokens['{categoryId}'];
			}

			// Check for alias
			if (isset($tokens['{categoryAlias}']))
			{
				return $this->getCategoryIdFromAlias($tokens['{categoryAlias}']);
			}

			// Check for path
			if (isset($tokens['{categoryPath}']))
			{
				return $this->getCategoryIdFromPath($tokens['{categoryPath}']);
			}
		}
		else
		{
			return;
		}
	}

	public function getItemid($variables)
	{
		$route = K2HelperRoute::getCategoryRoute($variables['id']);
		parse_str($route, $result);
		$Itemid = isset($result['Itemid']) ? $result['Itemid'] : '';

		return $Itemid;
	}

	private function getCategoryIdFromAlias($alias)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id')->from('#__k2_categories')->where('alias = ' . $db->q($alias));
		$application = JFactory::getApplication();

		if ($application->isClient('site') && $application->getLanguageFilter())
		{
			$query->where($db->qn('language') . ' IN(' . $db->q('*') . ', ' . $db->q($this->getLanguage()) . ')');
		}
		$db->setQuery($query);
		$id = $db->loadResult();

		return $id;
	}

	private function getCategoryIdFromPath($path)
	{
		$parts = explode('/', $path);
		$alias = end($parts);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id')->select('parent')->from('#__k2_categories')->where($db->qn('alias') . ' = ' . $db->q($alias));
		$db->setQuery($query);
		$categories = $db->loadObjectList();

		if (count($categories) == 1)
		{
			return $categories[0]->id;
		}

		$id = null;

		foreach ($categories as $category)
		{
			if ($category->parent == 0)
			{
				$id = $category->id;

				break;
			}
			JModelLegacy::addIncludePath(JPATH_SITE . '/plugins/route66/k2/models');
			$model = JModelLegacy::getInstance('K2', 'Route66Model', array('ignore_request' => true));
			$category->path = $model->getCategoryPath($category->id, $category->parent, array($alias));

			if ($category->path == $path)
			{
				$id = $category->id;

				break;
			}
		}

		return $id;
	}
}
