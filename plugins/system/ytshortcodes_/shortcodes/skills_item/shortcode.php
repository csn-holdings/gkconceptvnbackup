<?php 
/*
* @package   YouTech Shortcodes
* @author    YouTech Company http://smartaddons.com/
* @copyright Copyright (C) 2015 YouTech Company
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/
defined('_JEXEC') or die;
function skills_itemYTShortcode($atts, $content = null){
	global $no_number;
	extract(ytshortcode_atts(array(
		"title"		=> '',
		"link"      => '#', 
		"number"	=> '',
	), $atts));
	$css = '';
	$id = uniqid('skills_item_').rand().time();
	$css .= '#'.$id.' .bar{width:'. $number .'%}';
 
 $lang = JFactory::getLanguage();
 $languages = JLanguageHelper::getLanguages('lang_code');
 $languageCode = $languages[ $lang->getTag() ]->sef;

	$skill_item  =  "<div class='form-group' id='".$id."'>";
	$skill_item .=  "<a href='".JURI::base()."index.php/".$languageCode.trim($link)."'><strong>".$title."</strong></a>";
	$skill_item .=   ($no_number != 'no' || $no_number != 'No') ? "<span class='pull-right'>".$number."%</span>" : '' ;
	$skill_item .=   "<div class='progress progress-danger active'> <div class='bar'></div> </div>";
	$skill_item .=  "</div>";
	$doc = JFactory::getDocument();
	$doc->addStyleDeclaration($css);
	return $skill_item;
}
?>