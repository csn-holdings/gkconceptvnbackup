<?php
	defined('_JEXEC') or die('Restricted access');
	
	$product = $viewData['product'];
	$position = $viewData['position'];
	$customTitle = isset($viewData['customTitle']) ? $viewData['customTitle'] : false;;
	if (isset($viewData['class'])) {
		$class = $viewData['class'];
	} else {
		$class = 'product-fields';
	}
	
	if (!empty($product->customfieldsSorted[$position]))
	{
?>
		<div class="module box2 <?php echo $class ?>">
			<?php
				if ($customTitle and isset($product->customfieldsSorted[$position][0]))
				{
			?>
			<h3 class="modtitle product-fields-title-wrapper">
				<span class="product-fields-title">
					<?php echo vmText::_('Sản Phẩm Tương Tự') ?>
				</span>
			</h3>

			<div class="modcontent product-fields-content-wrapper">

			<?php
				}
					$i = 0;
					$custom_title = null;
					foreach ($product->customfieldsSorted[$position] as $key => $field)
					{
						if($key >3)
						{
							break;
						}
						
						if ($field->is_hidden || empty($field->display))
							continue;
				?>
						<div class="product-field product-field-type-<?php echo $field->field_type ?>">
							<?php if (!$customTitle and $field->custom_title != $custom_title and $field->show_title) { ?>
								<span class="product-fields-title-wrapper">
						<span class="product-fields-title">
							<strong><?php echo vmText::_($field->custom_title) ?></strong>
						</span>
						<?php
							if ($field->custom_tip)
							{
								echo JHtml::tooltip(vmText::_($field->custom_tip), vmText::_($field->custom_title), 'tooltip.png');
							}
						?>
					</span>
								<?php
							}
								if (!empty($field->display))
								{
									?>
									<div class="product-field-display">
										<?php echo $field->display ?>
									</div>
									<style>
										.prices-wrap {
											display: none;
										}
										.ratingbox.dummy {
											display: none;
										}
										.related-info a {
											font-size: 14px;
											font-weight: 700;
											color: #FF7600;
										}
									</style>
									<?php
								}
							?>
						</div>
						<?php
						$custom_title = $field->custom_title;
						$key ++;
					}
				?>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php
	} ?>
