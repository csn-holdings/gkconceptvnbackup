<?php
	defined('_JEXEC') or die('Restricted access');
	$product = $viewData['product'];
	$currency = $viewData['currency'];
	$position = $viewData['position'];
	foreach ($product->customfieldsSorted['addtocart'] as $fied)
	{
		if ($fied->custom_title == 'other_promotions')
		{
			$da = $fied->customfield_value;
		}
	}

	if (empty($da))
	{
		return;
	}
?>
<div class="row">
	<div class="iq-card">
		<div class="iq-card-header d-flex justify-content-between">
			<div class="iq-header-title">
				<h4 class="card-title">Các Uư Đãi khác</h4>
			</div>
		</div>
		<div class="iq-card-body">
			<ul class="iq-timeline">
				<?php echo $da; ?>
			</ul>
		</div>
	</div>
</div>
