<?php
	defined('_JEXEC') or die('Restricted access');
	
	if (empty($this->product)) {
		echo vmText::_('COM_VIRTUEMART_PRODUCT_NOT_FOUND');
		echo '<br /><br />  ' . $this->continue_link_html;
		return;
	}
	
	echo shopFunctionsF::renderVmSubLayout('askrecomjs', array('product' => $this->product));
	
	if (vRequest::getInt('print', false))
	{ ?>
<body onload="javascript:print();">
<?php } ?>

<div class="productdetails-view productdetails">
	<div class="vm-product-container">
		<div class="col-md-6 vm-product-images-container">
			<?php
				echo $this->loadTemplate('images');
			?>
			<?= shopFunctionsF::renderVmSubLayout('product_description2', array('product' => $this->product, 'position' => 'related_products', 'class' => 'product-related-products', 'customTitle' => true)); ?>
		</div>

		<div class="col-md-6 vm-product-details-container">
			<h1 class="product_title" itemprop="name"><?php echo $this->product->product_name; ?></h1>
			<?= $this->product->event->afterDisplayTitle ?>

			<div class="product_tool">
				<?php
					echo $this->edit_link;
				?>
				
				<?php
					if (VmConfig::get('show_emailfriend') || VmConfig::get('show_printicon') || VmConfig::get('pdf_icon')) {
						?>
						<div class="icons">
							<?php
								$link = 'index.php?tmpl=component&option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $this->product->virtuemart_product_id;
								
								echo $this->linkIcon($link . '&format=pdf', 'COM_VIRTUEMART_PDF', 'pdf_button', 'pdf_icon', false);
								
								echo $this->linkIcon($link . '&print=1', 'COM_VIRTUEMART_PRINT', 'printButton', 'show_printicon', false, true, false, 'class="printModal"');
								
								$MailLink = 'index.php?option=com_virtuemart&view=productdetails&task=recommend&virtuemart_product_id=' . $this->product->virtuemart_product_id . '&virtuemart_category_id=' . $this->product->virtuemart_category_id . '&tmpl=component';
								
								echo $this->linkIcon($MailLink, 'COM_VIRTUEMART_EMAIL', 'emailButton', 'show_emailfriend', false, true, false, 'class="recommened-to-friend"');
							?>
							<div class="clearfix"></div>
						</div>
						<?php
					}
				?>
			</div>
			
			<?php
				echo shopFunctionsF::renderVmSubLayout('prices2', array('product' => $this->product, 'currency' => $this->currency));
			?>
			<div class="clearfix"></div>
			<?php
				echo shopFunctionsF::renderVmSubLayout('addtocart', array('product' => $this->product));
				
				echo shopFunctionsF::renderVmSubLayout('call_api_boxme_fee', array('product' => $this->product, 'currency' => $this->currency));
			?>
			<div class="clearfix"></div>
			<?php
				echo shopFunctionsF::renderVmSubLayout('countdown2', array('product' => $this->product, 'currency' => $this->currency));
			?>
			<?php
				
				if (!empty($this->product->product_s_desc)) {
					?>
					<div class="product-short-description">
						<h3>Điểm Nỗi Bật:</h3>
						<?= nl2br($this->product->product_s_desc); ?>
					</div>
					<?php
				}
			?>
			<div class="product-other-container">
				<div class="display_payment">
					<?php
//						if (is_array($this->productDisplayShipments))
//						{
//							foreach ($this->productDisplayShipments as $productDisplayShipment) {
//								echo $productDisplayShipment . '';
//							}
//						}
//						if (is_array($this->productDisplayPayments)) {
//							foreach ($this->productDisplayPayments as $productDisplayPayment) {
//								echo $productDisplayPayment . '';
//							}
//						}
						
						echo shopFunctionsF::renderVmSubLayout('progress_sold', array('product' => $this->product, 'currency' => $this->currency));
					?>
					<div class="row">
						<div class="col-md-6">
							<?= shopFunctionsF::renderVmSubLayout('other_promotions', array('product' => $this->product, 'currency' => $this->currency)); ?>
						</div>
						<div class="col-md-6"><?= shopFunctionsF::renderVmSubLayout('usp', array('product' => $product, 'currency' => $currency)); ?></div>
					</div>
					<?= shopFunctionsF::renderVmSubLayout('product_buy_together', array('product' => $this->product, 'currency' => $this->currency)); ?>
				</div>

				<div class="display-ask-mf">
					<?php
						if (VmConfig::get('show_manufacturers', 1) && !empty($this->product->virtuemart_manufacturer_id)) {
							echo $this->loadTemplate('manufacturer');
						}
					?>
					<?php
						if (VmConfig::get('ask_question', 0) == 1) {
							$askquestion_url = JRoute::_('index.php?option=com_virtuemart&view=productdetails&task=askquestion&virtuemart_product_id=' . $this->product->virtuemart_product_id . '&virtuemart_category_id=' . $this->product->virtuemart_category_id . '&tmpl=component', FALSE);
							?>
							<div class="ask-a-question">
								<a class="question" href="<?php echo $askquestion_url ?>" rel="nofollow"><i
									class="fa fa-question-circle"></i><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_ENQUIRY_LBL') ?>
								</a>
							</div>
							<?php
						}
					?></div>

				<div class="display-nav">
					<?php
						
						if (VmConfig::get('product_navigation', 1)) {
							?>
							<div class="product-neighbours">
								<?php
									
									if (!empty($this->product->neighbours ['previous'][0])) {
										$prev_link = JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $this->product->neighbours ['previous'][0] ['virtuemart_product_id'] . '&virtuemart_category_id=' . $this->product->virtuemart_category_id, FALSE);
										echo JHtml::_('link', $prev_link, $this->product->neighbours ['previous'][0]
										['product_name'], array('rel' => 'prev', 'class' => 'previous-page', 'data-dynamic-update' => '1'));
									}
									
									if (!empty($this->product->neighbours ['next'][0])) {
										$next_link = JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $this->product->neighbours ['next'][0] ['virtuemart_product_id'] . '&virtuemart_category_id=' . $this->product->virtuemart_category_id, FALSE);
										echo JHtml::_('link', $next_link, $this->product->neighbours ['next'][0] ['product_name'], array('rel' => 'next', 'class' => 'next-page', 'data-dynamic-update' => '1'));
									}
								?>
								<div class="clear"></div>
							</div>
						<?php }
					?>
					
					<?php
						if ($this->product->virtuemart_category_id) {
							$catURL = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $this->product->virtuemart_category_id, FALSE);
							$categoryName = vmText::_($this->product->category_name);
						} else {
							$catURL = JRoute::_('index.php?option=com_virtuemart');
							$categoryName = vmText::_('COM_VIRTUEMART_SHOP_HOME');
						}
					?>
					<div class="back-to-category">
						<a href="<?php echo $catURL ?>" class="product-details"
						   title="<?php echo $categoryName ?>"><?php echo vmText::sprintf('COM_VIRTUEMART_CATEGORY_BACK_TO', $categoryName) ?></a>
					</div>
				</div>
				<?php
					echo $this->product->event->beforeDisplayContent; ?>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="vm-product-container">
		
		<?php (isset($this->product->customfieldsSorted['related_products'])) ? $col_ptabs = 'col-md-9' : $col_ptabs = 'col-md-12'; ?>
		<div class="col-md-3 product-related">
			<?php echo shopFunctionsF::renderVmSubLayout('related_products', array('product' => $this->product, 'position' => 'related_products', 'class' => 'product-related-products', 'customTitle' => true)); ?>
			<?php echo shopFunctionsF::renderVmSubLayout('vendor_store', array('product' => $this->product, 'currency' => $this->currency)); ?>
		</div>
		<div class="<?php echo $col_ptabs; ?> product-tabs">
			<!-- Nav tabs -->
			<ul id="addreview" class="nav nav-tabs clearfix" role="tablist">
				<li role="presentation" class="clearfix active"><a href="#home" aria-controls="home" role="tab"
																   data-toggle="tab"><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_DESC_TITLE') ?></a>
				</li>
				<li role="presentation"><a href="#add-reviews" aria-controls="add-reviews" role="tab"
										   data-toggle="tab"><?php echo vmText::_('COM_VIRTUEMART_REVIEWS') ?></a></li>
				<li role="presentation"><a href="#messages" aria-controls="messages" role="tab"
										   data-toggle="tab">Others</a></li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content clearfix">
				<div role="tabpanel" class="tab-pane clearfix active" id="home">
					<?php // Product Description
						if (!empty($this->product->product_desc)) {
							?>
							<div class="product-description">
								<?php /** @todo Test if content plugins modify the product description */ ?>
								<?php echo $this->product->product_desc; ?>
							</div>
							<?php
						} // Product Description END
					?>

				</div>
				<div role="tabpanel" class="tab-pane clearfix" id="add-reviews">
					<?php // onContentAfterDisplay event
						echo $this->product->event->afterDisplayContent;
						echo $this->loadTemplate('reviews'); ?>
				</div>
				<div role="tabpanel" class="tab-pane clearfix" id="messages">
					<?php
						// Product Packaging
						$product_packaging = '';
						if ($this->product->product_box) {
							?>
							<div class="product-box">
								<?php
									echo vmText::_('COM_VIRTUEMART_PRODUCT_UNITS_IN_BOX') . $this->product->product_box;
								?>
							</div>
						<?php } // Product Packaging END
						echo shopFunctionsF::renderVmSubLayout('customfields', array('product' => $this->product, 'position' => 'ontop'));
						echo shopFunctionsF::renderVmSubLayout('customfields', array('product' => $this->product, 'position' => 'normal'));
						echo shopFunctionsF::renderVmSubLayout('customfields', array('product' => $this->product, 'position' => 'onbot'));
					?>

				</div>
			</div>
			<div class="clearfix"></div>
			<?php echo shopFunctionsF::renderVmSubLayout('related_categories', array('product' => $this->product, 'position' => 'related_categories', 'class' => 'product-related-categories')); ?>
		</div>

	</div>


</div>


<div class="vm-product-container">
	<div class="col-md-12 product-tabs-category">
		<?php
			// Show child categories
			if (VmConfig::get('showCategory', 1)) {
				echo $this->loadTemplate('showcategory');
			}
			
			$j = 'jQuery(document).ready(function($) {
	Virtuemart.product(jQuery("form.product"));

	$("form.js-recalculate").each(function(){
		if ($(this).find(".product-fields").length && !$(this).find(".no-vm-bind").length) {
			var id= $(this).find(\'input[name="virtuemart_product_id[]"]\').val();
			Virtuemart.setproducttype($(this),id);

		}
	});
});';
			//vmJsApi::addJScript('recalcReady',$j);
			
			/** GALT
			 * Notice for Template Developers!
			 * Templates must set a Virtuemart.container variable as it takes part in
			 * dynamic content update.
			 * This variable points to a topmost element that holds other content.
			 */
			$j = "Virtuemart.container = jQuery('.productdetails-view');
Virtuemart.containerSelector = '.productdetails-view';";
			
			vmJsApi::addJScript('ajaxContent', $j);
			
			echo vmJsApi::writeJS();
			
			if ($this->product->prices['salesPrice'] > 0) {
				echo shopFunctionsF::renderVmSubLayout('snippets', array('product' => $this->product, 'currency' => $this->currency, 'showRating' => $this->showRating));
			}
		
		?>
	</div>
</div>



