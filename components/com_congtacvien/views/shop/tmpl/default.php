<?php
/**
 * @package    api
 * @subpackage C:
 * @author     Hau Pham {@link jooext.com}
 * @author     Created on 02-Oct-2017
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

//var_dump($this->vendor);

?>
<style>

    .container-logo {

    }
    .description {
        margin: 10px 5px;
    }
    .card-img-top{
        width: auto;height: 15rem;
        vertical-align:middle; horiz-align: center;
        margin: 10px auto;
    }
</style>
<div class="" ng-controller="MainController">

    <toaster-container toaster-options="{'position-class': 'toast-bottom-right', 'progress-bar': true, 'time-out':2000}"></toaster-container>


    <h1><?php echo $this->vendor->customtitle != "" ? $this->vendor->customtitle : $this->vendor->vendor_store_name; ?></h1>

    <div class="row">
        <div class="col-md-3">
            <div ng-if="vendor.hasBanner" class="container-logo"><img class="img-rounded" height="10rem" src="<?php echo $this->vendor->file_url;?>"/></div>
        </div>
        <div class="col-md-9">
            <div class="description"><?php echo $this->vendor->vendor_store_desc;?></div>
        </div>
    </div>

    <div class="clearfix">&nbsp;</div>

    <div class="row toolbar">
        <div class="col-md-12">
            <form class="form-inline my-2 my-lg-0" ng-submit="searchInShop()">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="inputProductGroup" class="col-sm-4 col-form-label">Nhóm sản phẩm</label>
                        <div class="col-sm-6">
                            <select ng-model="searchData.catid" id="inputProductGroup" ng-change="searchInShop()">
                                <option ng-value="0">--- Tất cả ---</option>
                                <option ng-repeat="category in categories" ng-value="category.id">{{category.text}} <span class="font-italic font-weight-bold">({{category.total}})</span> </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <input ng-model="searchData.searchString" class="form-control mr-sm-2" type="search" placeholder="Tìm trong cửa hàng" aria-label="Tìm trong cửa hàng">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Tìm kiếm</button>
                </div>
            </form>
        </div>
    </div>

    <div class="clearfix">&nbsp;</div>

    <div class="ph-item" ng-if="status.loading">
        <div class="ph-col-12">
            <div class="ph-picture"></div>
            <div class="ph-row">
                <div class="ph-col-6 big"></div>
                <div class="ph-col-4 empty big"></div>
                <div class="ph-col-2 big"></div>
                <div class="ph-col-4"></div>
                <div class="ph-col-8 empty"></div>
                <div class="ph-col-6"></div>
                <div class="ph-col-6 empty"></div>
                <div class="ph-col-12"></div>
            </div>
        </div>
    </div>


    <ul class="row list-products auto-clear equal-container product-grid" ng-if="!status.loading">
        <li class="product-item  col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1" ng-repeat="product in products">
            <div class="vendor-product"/>
            
            <div class="product-inner equal-element">
                <div class="product-top">
                    <div class="flash">
            													<span class="onnew">
            														<span class="text">
            															new
            														</span>
            													</span>
                    </div>
                    <div class="yith-wcwl-add-to-wishlist">
                        <div class="yith-wcwl-add-button">
                            <a href="#">Add to Wishlist</a>
                        </div>
                    </div>
                </div>
                <div class="product-thumb">
                    <div class="thumb-inner">
                        <a ng-href="{{product.link}}" title="{{product.product_name}}">
                            <img ng-src="/{{product.imageUrl}}" alt="img">
                        </a>
                    </div>
            
                </div>
                <div class="">
                    <h5 class="product-name product_title">
                        <a ng-href="{{product.link}}" title="{{product.product_name}}">{{product.product_name}}</a>
                    </h5>
                    <div class="group-info">
                        <div class="stars-rating">
                            <div class="star-rating">
                                <span class="star-3"></span>
                            </div>
                            <div class="count-star">
                                (3)
                            </div>
                        </div>
                        <div class="price">
                            <del>
                                €65
                            </del>
                            <ins>
                                €45
                            </ins>
                        </div>
                    </div>
                </div>
                <div class="loop-form-add-to-cart">
                    <form class="cart">
                        <div class="single_variation_wrap">
                            <div class="quantity">
                                <div class="control">
                                    <a class="btn-number qtyminus quantity-minus" href="#">-</a>
                                    <input type="text" data-step="1" data-min="0" value="1"
                                           title="Qty" class="input-qty qty" size="4">
                                    <a href="#" class="btn-number qtyplus quantity-plus">+</a>
                                </div>
                            </div>
                            <button class="single_add_to_cart_button button">Cho vào giỏ
                            </button>
                        </div>
                        <div class="variations">
                            <div class="variation">
                                <label class="variation-label">
                                    Capacity:
                                </label>
                                <div class="variation-value">
                                    <a href="#" class="change-value text over">
                                        <span>10ml</span>
                                    </a>
                                    <a href="#" class="change-value text active">
                                        <span>20ml</span>
                                    </a>
                                    <a href="#" class="change-value text">
                                        <span>50ml</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </li>
    </ul>

</div>

<script>
    myApp = angular.module("myApp", ['toaster', 'ui.bootstrap']);

    myApp.controller('MainController', ['$scope', '$http', 'toaster', '$interval', function ($scope, $http, toaster, $interval) {

        $scope.vendor = {
            id: <?php echo $this->vendor->virtuemart_vendor_id;?>,
            hasBanner: <?php echo ($this->vendor->file_url == "") ? '0' : "1";?>
        };

        $scope.products = [];
        $scope.status = {
            loading: false
        };

        $scope.searchData = {
            catid: 0,
            searchString: ""
        };

        $scope.categories = [];

        $scope.getProducts = function () {
            $scope.status.loading = true;
            let url = 'index.php?option=com_congtacvien&task=shop.getfrontvendorproducts&vendor_id=<?php echo $this->vendor->virtuemart_vendor_id;?>';
            url += "&catid="+ $scope.searchData.catid + "&search="+$scope.searchData.searchString;
            $http.get(url)
                .then(function(response){
                    if (response.status == 200) {
                        if (response.data.success) {
                            $scope.products = response.data.data;
                            $scope.categories = response.data.categories;
                            $scope.status.loading = false;
                        } else {
                            toaster.pop("error", response.data.message, "", 0);
                            $scope.status.loading = false;
                        }
                    } else {
                        toaster.pop("error", response.statusText, "", "");
                    }
                });

        };

        $scope.getProducts();


        $scope.addToCart = function () {

        };

        $scope.searchInShop = function () {

            console.log($scope.searchData);
            $scope.getProducts();

        };

        angular.element(document).ready(function() {

// ------------------------Quick view----------------------------------------
            function kt_get_scrollbar_width() {
                var $inner = jQuery('<div style="width: 100%; height:200px;">test</div>'),
                    $outer = jQuery('<div style="width:200px;height:150px; position: absolute; top: 0; left: 0; visibility: hidden; overflow:hidden;"></div>').append($inner),
                    inner = $inner[0],
                    outer = $outer[0];
                jQuery('body').append(outer);
                var width1 = parseFloat(inner.offsetWidth);
                $outer.css('overflow', 'scroll');
                var width2 = parseFloat(outer.clientWidth);
                $outer.remove();
                return (width1 - width2);
            }

            function quickview_popup_product() {
                var window_size = parseFloat(jQuery('body').innerWidth());
                window_size += kt_get_scrollbar_width();
                if (window_size > 992) {
                    angular.element(document).on('click', '.quick-wiew-button-product', function (event) {
                        event.preventDefault();

                        console.log(event);
                        angular.element.magnificPopup.open({
                            items: {
                                src: 'hau <div class="kt-popup-quickview "><div class="details-thumb"><div class="slider-product slider-for"><div class="details-item"><img src="/templates/je_mekozzy/images/popup-quickview-item-1.jpg" alt="img"></div><div class="details-item"><img src="/templates/je_mekozzy/images/popup-quickview-item-1.jpg" alt="img"></div><div class="details-item"><img src="assets/images/popup-quickview-item-1.jpg" alt="img"></div><div class="details-item"><img src="assets/images/popup-quickview-item-1.jpg" alt="img"></div></div><div class="slider-product-button slider-nav nav-center"><div class="details-item"><img src="assets/images/popup-quickview-item-1.jpg" alt="img"></div><div class="details-item"><img src="assets/images/popup-quickview-item-1.jpg" alt="img"></div><div class="details-item"><img src="assets/images/popup-quickview-item-1.jpg" alt="img"></div><div class="details-item"><img src="assets/images/popup-quickview-item-1.jpg" alt="img"></div></div></div><div class="details-infor"><h1 class="product-title">Eclipse Pendant Light</h1><div class="stars-rating"><div class="star-rating"><span class="star-5"></span></div><div class="count-star">(7)</div></div><div class="availability">availability:<a href="#">in Stock</a></div><div class="price"><span>€45</span></div><div class="product-details-description"><ul><li>Vestibulum tortor quam</li><li>Imported</li><li>Art.No. 06-7680</li></ul></div><div class="variations"><div class="attribute attribute_color"><div class="color-text text-attribute">Color:<span>White/</span><span>Black/</span><span>Teal/</span><span>Brown</span></div><div class="list-color list-item"><a href="#" class="color1"></a><a href="#" class="color2"></a><a href="#" class="color3 active"></a><a href="#" class="color4"></a></div></div><div class="attribute attribute_size"><div class="size-text text-attribute">Size:</div><div class="list-size list-item"><a href="#" class="">xs</a><a href="#" class="">s</a><a href="#" class="active">m</a><a href="#" class="">l</a><a href="#" class="">xl</a><a href="#" class="">xxl</a></div></div></div><div class="group-button"><div class="yith-wcwl-add-to-wishlist"><div class="yith-wcwl-add-button"><a href="#">Add to Wishlist</a></div></div><div class="size-chart-wrapp"><div class="btn-size-chart"><a id="size_chart" href="assets/images/size-chart.jpg" class="fancybox"  target="_blank">View Size Chart</a></div></div><div class="quantity-add-to-cart"><div class="quantity"><div class="control"><a class="btn-number qtyminus quantity-minus" href="#">-</a><input type="text" data-step="1" data-min="0" value="1" title="Qty" class="input-qty qty" size="4"><a href="#" class="btn-number qtyplus quantity-plus">+</a></div></div><button class="single_add_to_cart_button button">Cho vào giỏ</button></div></div></div></div>',
                                type: 'inline'
                            }
                        });
                        slick_quickview_popup_product();
                        return false;
                    });
                }
            };

            function slick_quickview_popup_product() {
                angular.element('.slider-for').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    asNavFor: '.slider-nav'
                });
                angular.element('.slider-nav').slick({
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    asNavFor: '.slider-for',
                    dots: false,
                    focusOnSelect: true,
                    infinite: true,
                    prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                    nextArrow: '<i class="fa fa-angle-right " aria-hidden="true"></i>',
                });
            };

            quickview_popup_product();
        });


    }]);


</script>