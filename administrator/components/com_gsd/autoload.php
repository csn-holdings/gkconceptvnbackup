<?php

/**
 * @package         Google Structured Data
 * @version         4.7.0 Free
 * 
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2018 Tassos Marinos All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
*/

defined('_JEXEC') or die('Restricted access');

// Register Convert Form namespace
JLoader::registerNamespace('GSD', JPATH_ADMINISTRATOR . '/components/com_gsd/GSD', false, false, 'psr4');

?>